<?php
  /* Settings */
  $limit = 10;
  $radius = 1000;

  /*
    //Test info

    $_POST['lat'] = "3.419394220652494";
    $_POST['lng'] = "-76.5222430229187";
    $_POST['qry'] = "barrio";
    $_POST['type'] = 'explore';

  */

  $config = include "../config.php";
  require_once '../../vendor/autoload.php';
  use Jcroll\FoursquareApiClient\Client\FoursquareClient;

  $errors = array();

  if (empty($_POST['lat'])) {
      $errors['lat'] = 'Error';
  }
  if (empty($_POST['lng'])) {
      $errors['lng'] = 'Error';
  }
  if (empty($_POST['qry'])) {
      $errors['qry'] = ' Error';
  }
  if (empty($_POST['type'])) {
      $errors['type'] = ' Error';
  }

  if (empty($errors)) {
    $client = FoursquareClient::factory(array(
      'client_id'     => $config['client_id'],    // required
      'client_secret' => $config['client_secret'] // required
    ));


    $lat = $_POST['lat'];
    $lng = $_POST['lng'];
    $query = $_POST['qry'];


    if(strcmp($_POST['type'], 'search') == 0){
      $command = $client->getCommand('venues/search', array(
        'll' => $lat.', '.$lng,
        'query' => $query,
        'limit' => $limit,
        'radius' => $radius
      ));
    }else if(strcmp($_POST['type'], 'explore') == 0){
      $command = $client->getCommand('venues/explore', array(
        'll' => $lat.', '.$lng,
        'limit' => $limit,
        'radius' => $radius
      ));
    }

    try{
      $results = $command->execute(); // returns an array of results
      //fetch results
      $venues = array();

      if(isset($results["response"]["groups"][0]["items"])){
        $r_venues = $results["response"]["groups"][0]["items"];
        for($i=0; $i<count($r_venues); $i++){
          array_push($venues, $r_venues[$i]['venue']);
        }
      }else if(isset($results["response"]["venues"])){
        $venues = $results["response"]["venues"];
      }else{
        echo "System error";
      }

      $output = array();
      foreach($venues as $venue){
        $lat = $venue['location']['lat']; /* Venue Latitude */ 
        $lng = $venue['location']['lng']; /* Venue Longitude */ 
        $name = $venue['name']; /* Venue Name */ 
        $location = "";
        if(isset($venue['location']['address'])){
          $location = $venue['location']['address']; /* Venue Location */ 
        }else{
          $location = "Not available";
        }
        $city = "";
        if(isset($venue['location']['city'])){
          $city = $venue['location']['city'];
        }else{
          $city = "";
        }
        $state = "";
        if(isset($venue['location']['state'])){
          $state = $venue['location']['state'];
        }else{
          $state = "";
        }
        $country = "";
        if(isset($venue['location']['country'])){
          $country = $venue['location']['country'];
        }else{
          $country = "";
        }
        $cats = $venue['categories']; /* Venue Categories Temp */
        $categories = array(); /* Venue Categories */
        foreach($cats as $cat){
          array_push($categories, $cat['name']);
        }
        $images = getVenueImages($venue['id']);  /* Venue images array */
        $rating = getSingleVenue($venue['id']); 
        if(isset($rating['rating'])){ 
          $rating = $rating['rating']; /* Venue rating value */
        }else{
          $rating = "Not available"; /* Venue rating not available */
        }
        
        array_push($output, array(
          'lat' => $lat,
          'lng' => $lng,
          'name' => $name,
          'location' => $location,
          'city' => $city,
          'state' => $state,
          'country' => $country,
          'categories' => $categories,
          'rating' => $rating,
          'images' => $images
        ));
      }

      echo json_encode($output);
    }
    catch(Exception $e){
      echo "The search didn't found places or business";
    }
  }else{
    echo "Error";
  }

  function getSingleVenue($venueId){
    $config = include "../config.php";
    $client = FoursquareClient::factory(array(
      'client_id'     => $config['client_id'],    // required
      'client_secret' => $config['client_secret'] // required
    ));
    $command = $client->getCommand('venues', array(
        'venue_id' => $venueId
      ));
    try{
      $results = $command->execute(); // returns an array of results
      return $results['response']['venue'];
    }catch(Exception $e){
      return "Error";
    }
  }

  function getVenueImages($venueId){
    $config = include "../config.php";
    $client = FoursquareClient::factory(array(
      'client_id'     => $config['client_id'],    // required
      'client_secret' => $config['client_secret'] // required
    ));
    $command = $client->getCommand('venues/photos', array(
        'venue_id' => $venueId
      ));
    try{
      $results = $command->execute(); // returns an array of results
      $photos = $results['response']['photos']['items'];
      $r_photos = array();

      $limit_images = 5;
      $count = 0; //Limits ammount of images in array
      foreach($photos as $photo){
        if($count < $limit_images){
          array_push($r_photos, $photo['prefix'].'300x300'.$photo['suffix']);
          $count++;
        }else{
          break;
        }
      }
      return $r_photos;
    }catch(Exception $e){
      return "Error";
    }
  }

?>
