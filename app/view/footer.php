    <script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="js/foundation.min.js"></script>
    <script src="js/foundation/foundation.accordion.js"></script>
    <script src="js/foundation/foundation.topbar.js"></script>
    <script src="js/vendor/unslider.min.js"></script>
    <script src="js/app/app.js"></script>
    <script>
      $(document).foundation();
    </script>
  </body>
</html>