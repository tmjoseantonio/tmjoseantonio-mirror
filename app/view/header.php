<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>FourSquare Web App | Jose Tovar</title>
    <link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" type="text/css" href="css/foundation-icons/foundation-icons.css">
    <link rel="stylesheet" type="text/css" href="js/vendor/scrollbar/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <script src="js/vendor/modernizr.js"></script>
    <script type="text/javascript"
      src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAsOGdAH-4VpRvQR23tqASVgZqvUbQtd6I&sensor=true">
    </script>
    <script src="js/app/geolocator.js"></script>
  </head>
  <body>