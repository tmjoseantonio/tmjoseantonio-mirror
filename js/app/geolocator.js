var map;
var markers = [];
var infowindow;
var myStyles =[
    {
        featureType: "poi",
        elementType: "labels",
        stylers: [
              { visibility: "off" }
        ]
    }
];

// Initializes the default map without geolocation
function initialize_ng() {

  var lat = 26.895964;
  var lng = -81.0762376;
  var mapOptions = {
    zoom: 3,
    center: new google.maps.LatLng(lat,lng),
    styles: myStyles,
    mapTypeControl: false, // Disable all default map controls
    panControl: false,
    zoomControl: false,
    scaleControl: false,
    streetViewControl: false
  };
  map = new google.maps.Map(document.getElementById('map-canvas'),
      mapOptions);
  setCenter(lat, lng, 0);
  // Create the DIV to hold the control and call the HomeControl() constructor
  // passing in this DIV.
  var zoomControlDiv = document.createElement('div');
  var zoomControl = new zoomInControl(zoomControlDiv, map);
  zoomControlDiv.index = 1;
  map.controls[google.maps.ControlPosition.TOP_RIGHT].push(zoomControlDiv);
}

// Initializes the default with geolocation
function initialize() {

  var mapOptions = {
    zoom: 16,
    styles: myStyles,
    mapTypeControl: false, //Disable all default map controls
    panControl: false,
    zoomControl: false,
    scaleControl: false,
    streetViewControl: false
  };
  map = new google.maps.Map(document.getElementById('map-canvas'),
      mapOptions);
  // Try HTML5 geolocation
  if(navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      setCenter(position.coords.latitude, position.coords.longitude, 1);

      var zoomControlDiv = document.createElement('div');
      var zoomControlA = new zoomInControl(zoomControlDiv, map);
      var zoomControlB = new zoomOutControl(zoomControlDiv, map);
      zoomControlDiv.index = 1;
      map.controls[google.maps.ControlPosition.LEFT_CENTER].push(zoomControlDiv);

    }, function() {
      initialize_ng();
    });
  } else {
    initialize_ng();
  }

}

function zoomInControl(controlDiv, map) {

  controlDiv.style.padding = '5px';

  // Set CSS for the control border.
  var controlUI = document.createElement('div');
  controlUI.style.backgroundColor = 'white';
  controlUI.style.borderStyle = 'solid';
  controlUI.style.width = '46px';
  controlUI.style.height = '46px';
  controlUI.style.paddingTop = '5px';
  controlUI.style.borderWidth = '2px';
  controlUI.style.marginBottom = '10px';
  controlUI.style.cursor = 'pointer';
  controlUI.style.textAlign = 'center';
  controlUI.title = 'Zoom In';
  controlDiv.appendChild(controlUI);

  // Set CSS for the control interior.
  var controlText = document.createElement('div');
  controlText.style.fontFamily = 'Verdana';
  controlText.style.fontSize = '30px';
  controlText.innerHTML = '<strong>+</strong>';
  controlUI.appendChild(controlText);

  // Setup the click event listeners: simply set the map to Chicago.
  google.maps.event.addDomListener(controlUI, 'click', function() {
    map.setZoom(map.getZoom()+1);
  });

}

function zoomOutControl(controlDiv, map) {

  controlDiv.style.padding = '5px';

  // Set CSS for the control border.
  var controlUI = document.createElement('div');
  controlUI.style.backgroundColor = 'white';
  controlUI.style.borderStyle = 'solid';
  controlUI.style.width = '46px';
  controlUI.style.height = '46px';
  controlUI.style.paddingTop = '5px';
  controlUI.style.borderWidth = '2px';
  controlUI.style.cursor = 'pointer';
  controlUI.style.textAlign = 'center';
  controlUI.title = 'Zoom Out';
  controlDiv.appendChild(controlUI);

  // Set CSS for the control interior.
  var controlText = document.createElement('div');
  controlText.style.fontFamily = 'Verdana';
  controlText.style.fontSize = '30px';
  controlText.innerHTML = '<strong>-</strong>';
  controlUI.appendChild(controlText);

  // Setup the click event listeners: simply set the map to Chicago.
  google.maps.event.addDomListener(controlUI, 'click', function() {
    map.setZoom(map.getZoom()-1);
  });

}

function setCenter(lat, lng, geo_flag){

  var pos = new google.maps.LatLng(lat, lng);
  var marker = false;

  if(geo_flag){ // if is showing the map with geo location
    map.setCenter(pos);
    addMarker(pos, 1);
    infowindow = new google.maps.InfoWindow({
      content: '<p style="text-align:center;margin:0;padding:0;">Click anywere on the map<br />to start looking for places</p>'
    });
    infowindow.open(map,markers[0]);
  }else{ // if is not showing the map with geo location
    marker = new google.maps.Marker();
    infowindow = new google.maps.InfoWindow({
      map: map,
      position: pos,
      content: '<p style="text-align:center;margin:0;padding:0;">We couldn\'t determine your location.<br />Click anywere on the map to start</p>'
    });
  }

  google.maps.event.addListener(map, 'click', function(event) {
    deleteMarkers(); // Remove previous markers
    addMarker(event.latLng, 1);
    if(map.getZoom() < 9){
      map.setZoom(9);
    }
    map.setCenter(event.latLng);
    getClosePlaces();
  });

  google.maps.event.addListener(markers[0], 'click', function() {
    infowindow.open(map,markers[0]);
  });

}

// Removes the markers from the map, but keeps them in the array.
function clearMarkers() {

  setAllMap(null);

}


function setAllMap(map) {

  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(map);
  }

}

// Deletes all markers in the array by removing references to them.
function deleteMarkers() {

  clearMarkers();
  markers = [];

}

// Add a marker to the map and push to the array. 
function addMarker(location, type) {

  var image = 'img/marker_green.png'; //Icon for viewer location marker
  var marker;
  if(type === 0){ // Set the default marker icon
    marker = new google.maps.Marker({
      position: location,
      map: map,
      zIndex: 100
    });
  }else{ // Set a green marker, this one is intended to show the center of the area to explore
    marker = new google.maps.Marker({
      position: location,
      map: map,
      icon:image,
      zIndex: 200
    });
  }
  markers.push(marker);

}

// Get the coordinates of the current center of the map
function getCoords(){

  var center = map.getCenter();
  center = [center.lat(), center.lng()];
  return center;

}


// Look for places close to the current location.
function getClosePlaces(){
  $('.loader').fadeIn(); // fade in the loader
  var coords = getCoords();
  $.ajax({
    type: "POST",
    url: "app/controller/controller.php",
    data: { lat: coords[0], lng: coords[1], qry: 'null', type:'explore' }
  })
  .done(function( msg ) {
      $('.loader').fadeOut(); // fade out the loader
      $('.p-places-btn').trigger('click');
      if(msg != "Error")
      {
        var result = jQuery.parseJSON(msg);
        var l_result = result.length;

        $('.p-places .accordion').html(' ');


        for(i=0;i<l_result;i++)
        {
          var placeImages = result[i]['images'];
          var images = "";
          if(placeImages.length > 0){
            var count = 0;
            while(count < placeImages.length){
              images+="<li><img src='"+placeImages[count]+"'></li>";
              count++;
            }
          }

          var placeStr = '<dd class="accordion-navigation"><a href="#panel'+i+'">'+result[i]['name']+'</a><div id="panel'+i+'" class="content"><h6>Location: '+result[i]['location']+', '+result[i]['city']+', '+result[i]['country']+'</h6><h6>Rating: '+result[i]['rating']+'</h6><h6>Category: '+result[i]['categories'][0]+'</h6><div class="location-images"><ul>'+images+'</ul></div></div></dd>';

          $('.p-places .accordion').append(placeStr);
          
          var myLatlng = new google.maps.LatLng(result[i]['lat'],result[i]['lng']);
          addMarker(myLatlng, 0);
        }
      }else{
        console.log(msg);
      }
  });

}

// Look for places near current location giving a keyword/phrase.
function getPlaces(query){
  $('.loader').fadeIn(); // fade in the loader
  var coords = getCoords();
  $.ajax({
    type: "POST",
    url: "app/controller/controller.php",
    data: { lat: coords[0], lng: coords[1], qry: query, type:'search' }
  })
    .done(function( msg ) {
      $('.loader').fadeOut(); // fade out the loader
      $('.p-places-btn').trigger('click');
      var result = jQuery.parseJSON(msg);
      result = result["response"]["venues"];
      var l_result = result.length;
      for(i=0;i<l_result;i++){
        var id = result[i]['id'];
        var place = result[i]['name'];
        var location = result[i]['location']['formattedAddress'][0]+'<br />';
        $('.p-places .accordion').append('<dd class="accordion-navigation"><a href="#panel'+i+'">'+place+'</a><div id="panel'+i+'" class="content"><h6>Location: <br />'+location+'</h6></div></dd>');
        var myLatlng = new google.maps.LatLng(result[i]['location']['lat'],result[i]['location']['lng']);
        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            title: place
        });
        google.maps.event.trigger(map, 'resize');
      }
    });
    
}

google.maps.event.addDomListener(window, 'load', initialize);