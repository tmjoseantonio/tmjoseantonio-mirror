(function($){
    $(window).load(function(){
        $(".p-places-wrap").mCustomScrollbar();
    });
})(jQuery);

$(document).foundation({
  accordion: {
    // specify the class used for active (or open) accordion panels
    active_class: 'active',
    // allow multiple accordion panels to be active at the same time
    multi_expand: false,
    // allow accordion panels to be closed by clicking on their headers
    // setting to false only closes accordion panels when another is opened
    toggleable: true
  }
});

$(window).load(function(){
  $('.loader').fadeOut();
});

var places = false;
$('.p-places-btn').click(function(){
  if(!places){
      $('.p-places').animate({right:0}, 250, function(){
        places = true;
      });
  }else{
      $('.p-places').animate({right:-250}, 250, function(){
        places = false;
      });
  }
});
$('.p-places-close').click(function(){
  if(!places){
      $('.p-places').animate({right:0}, 250, function(){
        places = true;
      });
  }else{
      $('.p-places').animate({right:-250}, 250, function(){
        places = false;
      });
  }
});