<?php include("app/view/header.php"); ?>

<nav class="top-bar" data-topbar>
  <ul class="title-area">
    <li class="name">
      <h1><a href="#">Foursquare web App - José Tovar</a></h1>
    </li>
     <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
    <!-- <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li> -->
  </ul>

  <section class="top-bar-section">
    <ul class="right">
      <li class="active"><a href="#" class="p-places-btn">Explore places</a></li>
    </ul>
  </section>
</nav>

  <div class="row fullscreen">
    <div id="map-canvas"></div>
  </div>

  <div class="p-places">
    <div class="p-places-close">X</div>
    <div class="p-places-wrap">
      <dl class="accordion" data-accordion>
        <dd class="accordion-navigation active"><a href="#panel0">No places yet</a><div id="panel0" class="content active"></div></dd>
      </dl>
    </div><!-- .p-places-wrap -->
  </div><!-- .places -->


  <div class="loader">
    <div class="loading"></div>
    <div class="loading-bg"></div>
  </div>
<?php include("app/view/footer.php"); ?>